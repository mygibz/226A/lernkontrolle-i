﻿using System;

namespace Lernkontrolle {
    class Extern : Person {
        public string unternehmen;

        public Extern(string vorname, string nachname, DateTime geburtsdatum, string geschlecht, string unternehmen, string strasse, string postleitzahl, string ort, string telefonnummer, string email, string telefontyp = "Mobil") : base(vorname, nachname, geburtsdatum, geschlecht, strasse, postleitzahl, ort, email, telefonnummer, telefontyp) {
            this.unternehmen = unternehmen;
        }

        public new void OutputPersonalDetails() {
            base.OutputPersonalDetails();
            Console.WriteLine($"Unternehmen: {unternehmen}");
        }
    }
}
