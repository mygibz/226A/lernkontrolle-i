﻿using System;

namespace Lernkontrolle {
    class Program {
        static void Main(string[] args) {
            // interner mitarbeiter erstellen
            Intern person = new Intern("Max", "Muster", DateTime.Now, "Männlich", "Management", "Musterstrasse", "6314", "Unterägeri", "noone@nowhere.com", "123 456 78 12");
            // daten ändern
            person.setAddress("Neustrasse", "614", "Neuhausen");
            person.setTelefon("548 645 84 31", "Mobil");
            // ausgeben
            person.OutputPersonalDetails();
        }
    }
}
