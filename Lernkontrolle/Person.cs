﻿using System;

namespace Lernkontrolle {
    class Person {
        private string _vorname;
        private string _nachname;
        private DateTime _geburtsdatum;
        private int _geschlecht;           // used as ID's (0: Unknown, 1: Male, 2: Female, 3:Non-Binary, 4:Other)

        private int _hauptEmail = 0;
        private int _haubtTelefonnummer = 0;
        private int _haubtAdresse = 0;
        private string[] _email = new string[100];        // Nummer, Typ
        private string[,] _telefonnummern = new string[100, 2];        // Nummer, Typ
        private string[,] _postadressen = new string[100, 3];        // Strasse, Postleitzahl, Ort

        public string geschlecht {
            get => IntToGender(_geschlecht);
            set => StringToGender(value);
        }

        public DateTime geburtsdatum {
            get => _geburtsdatum;
        }

        public string email {
            get => _email[_hauptEmail];
            set => setEmail(value);
        }
        public string telefonnummer {
            get => _telefonnummern[_haubtTelefonnummer, 0];
        }
        public string adresse {
            get => $"{_postadressen[_haubtAdresse, 0]} {_postadressen[_haubtAdresse, 1]} {_postadressen[_haubtAdresse, 2]}";
        }

        // Konstruktor, falls int für Geschlecht verwendet
        public Person(string vorname, string nachname, DateTime geburtsdatum, int geschlecht, string strasse, string postleitzahl, string ort, string email, string telefonnummer, string telefontyp = "Mobil") {
            _vorname = vorname;
            _nachname = nachname;
            _geburtsdatum = geburtsdatum;
            _geschlecht = geschlecht;
            AddPhoneNumber(telefontyp, telefonnummer);
            AddAddress(strasse, postleitzahl, ort);
            AddEmail(email);
        }
        // Konstruktor, falls string für Geschlecht verwendet
        public Person(string vorname, string nachname, DateTime geburtsdatum, string geschlecht, string strasse, string postleitzahl, string ort, string email, string telefonnummer, string telefontyp = "Mobil") {
            _vorname = vorname;
            _nachname = nachname;
            _geburtsdatum = geburtsdatum;
            _geschlecht = StringToGender(geschlecht);
            AddPhoneNumber(telefontyp, telefonnummer);
            AddAddress(strasse, postleitzahl, ort);
            AddEmail(email);
        }

        private int GetLastEntryPosition(string[,] array) {
            for (int i = 0; i < 100; i++) {
                if (array[i, 0] == null) {
                    return i;
                }
            }
            return -1;
        }

        public void AddEmail(string email) {
            int slot = Array.IndexOf(_email, null);

            _email[slot] = email;
        }

        public void AddPhoneNumber(string telefontyp, string telefonnummer) {
            int slot = GetLastEntryPosition(_telefonnummern);

            _telefonnummern[slot, 0] = telefonnummer;
            _telefonnummern[slot, 1] = telefontyp;
        }

        public void AddAddress(string strasse, string postleitzahl, string ort) {
            int slot = GetLastEntryPosition(_postadressen);

            _postadressen[slot, 0] = strasse;
            _postadressen[slot, 1] = postleitzahl;
            _postadressen[slot, 2] = ort;
        }

        public void setEmail(string email) {
            int result = -1;
            for (int i = 0; i < 100; i++)
                if (_email[i] == email)
                    result = i;    
            _hauptEmail = (result == -1) ? Array.IndexOf(_email, null) : result;
            AddEmail(email);
        }
        public void setTelefon(string nummer, string type) {
            int result = -1;
            for (int i = 0; i < 100; i++)
                if (_telefonnummern[i, 0] == nummer)
                    result = i;
            _haubtTelefonnummer = (result == -1) ? GetLastEntryPosition(_telefonnummern) : result;
            AddPhoneNumber(type, nummer);
        }
        public void setAddress(string strasse, string postleitzahl, string ort) {
            int result = -1;
            for (int i = 0; i < 100; i++)
                if (_postadressen[i, 0] == strasse && _postadressen[i, 1] == postleitzahl && _postadressen[i, 2] == ort)
                    result = i;
            _haubtAdresse = (result == -1) ? GetLastEntryPosition(_postadressen) : result;
            AddAddress(strasse, postleitzahl, ort);
        }

        // convert int to string
        private string IntToGender(int geschlecht = 0) {
            switch (geschlecht) {
                case 1:
                    return "Männlich";
                case 2:
                    return "Weiblich";
                case 3:
                    return "Non-Binary";
                default:
                    return "Unbekannt";
            }
        }
        // convert string to int
        private int StringToGender(string value = "") {
            switch (value) {
                case "Männlich":
                case "Mann":
                    return 1;
                case "Weiblich":
                case "Frau":
                    return 2;
                case "Non-Binary":
                case "Other":
                case "Anderes":
                    return 3;
                default:
                    return 0;
            }
        }

        // output personal details
        public void OutputPersonalDetails() {
            Console.WriteLine($"Vorname: {_vorname}");
            Console.WriteLine($"Nachname: {_nachname}");
            Console.WriteLine($"Email: {email}");
            Console.WriteLine($"Geburtsdatum: {_geburtsdatum.ToLongDateString()}");
            Console.WriteLine($"Geschlecht: {geschlecht}");
            Console.WriteLine($"Adresse: {adresse}");
            Console.WriteLine($"Telefonnummer: {telefonnummer}");
        }
    }
}
