﻿using System;

namespace Lernkontrolle {
    class Intern : Person {
        public string abteilung;
        private DateTime _eintrittsdatum;

        public DateTime eintrittsdatum {
            get => _eintrittsdatum;
        }

        // default constructor
        public Intern(string vorname, string nachname, DateTime geburtsdatum, string geschlecht, string abteilung, DateTime eintrittsdatum, string strasse, string postleitzahl, string ort, string email, string telefonnummer, string telefontyp = "Mobil") : base(vorname, nachname, geburtsdatum, geschlecht, strasse, postleitzahl, ort, email, telefonnummer, telefontyp) {
            this.abteilung = abteilung;
            _eintrittsdatum = eintrittsdatum;
        }
        // constructor if no join-date
        public Intern(string vorname, string nachname, DateTime geburtsdatum, string geschlecht, string abteilung, string strasse, string postleitzahl, string ort, string email, string telefonnummer, string telefontyp = "Mobil") : base(vorname, nachname, geburtsdatum, geschlecht, strasse, postleitzahl, ort, email, telefonnummer, telefontyp) {
            this.abteilung = abteilung;
            _eintrittsdatum = DateTime.UtcNow;
        }

        public new void OutputPersonalDetails() {
            base.OutputPersonalDetails();
            Console.WriteLine($"Abteilung: {abteilung}");
            Console.WriteLine($"Eintrittsdatum {eintrittsdatum.ToLongDateString()}");
        }
    }
}
